pragma solidity >=0.5.0;

contract Campaign {
    string public description;
    address private manager;
        
    uint public target;
    uint public amount = 0;
    
    uint private minimumContribution;
    
    uint totalContributors = 0;
    address [] private supporters;
    mapping(address => uint) contributions;
        
    ExpenditureRequest [] expenditureRequests;
    
    struct ExpenditureRequest {
        string description;
        bool completeStatus;
        uint value;
        address recipient;
        uint approvalCount;
        mapping(address => bool) supporterVotes;
    }
    
    modifier onlyManager() {
        require(msg.sender == manager);
        _;
    }
    
    modifier onlySupporter() {
        require(contributions[msg.sender] >= minimumContribution);
        _;
    }
    
    constructor(string memory _description, uint _target, uint _minimumContribution) public{
        require(_minimumContribution > 0);
        description = _description;
        manager = msg.sender;
        target = _target;
        minimumContribution = _minimumContribution;
    }
    
    function getBalance() public view returns(uint) {
        return address(this).balance;
    }
    
    function getMinimumContribution() public view returns(uint) {
        return minimumContribution;
    }
    
    function contribute() public payable {
        require(msg.value > 0);
        if (contributions[msg.sender] == 0) {
            totalContributors += 1;
        }
        
        uint previousContribution = contributions[msg.sender];
        contributions[msg.sender] += msg.value;
        
        if(contributions[msg.sender] >= minimumContribution && previousContribution < minimumContribution){
            supporters.push(msg.sender);
        }
        
        amount += msg.value;
    }
    
    function makeExpenditureRequest (string memory _description, address _recipient, uint _value) public onlyManager {
        require(address(this).balance >= _value);
        ExpenditureRequest memory newRequest = ExpenditureRequest(
            {
                description: _description,
                completeStatus: false,
                value: _value,
                recipient: _recipient,
                approvalCount: 0
                
            }); 
        expenditureRequests.push(newRequest);
    }
    
    function getExpenditureRequest(uint _id) public view returns
    (
        string memory _description, 
        bool _completeStatus,
        uint _value,
        address _recipient,
        uint _approvalCount
    ){
        ExpenditureRequest memory selectedRequest = expenditureRequests[_id];
        return (selectedRequest.description, selectedRequest.completeStatus, selectedRequest.value, selectedRequest.recipient, selectedRequest.approvalCount);
    }
    
    function approveExpenditureRequest(uint _id) public onlySupporter{
        ExpenditureRequest storage selectedRequest = expenditureRequests[_id];
        require(selectedRequest.supporterVotes[msg.sender] != true);
        
        selectedRequest.supporterVotes[msg.sender] = true;
        selectedRequest.approvalCount++;
    }
    
    function completeExpenditureRequest(uint id) public payable onlyManager {
        ExpenditureRequest storage selectedRequest = expenditureRequests[id];
        require(selectedRequest.completeStatus != true);
        require(selectedRequest.approvalCount >= supporters.length/2);
        
        address payable recipientAddress = address(uint160(selectedRequest.recipient));
        recipientAddress.transfer(selectedRequest.value);
        selectedRequest.completeStatus = true;
    }
}
